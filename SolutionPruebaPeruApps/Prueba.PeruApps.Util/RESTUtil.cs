﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prueba.PeruApps.Entity;
using Prueba.PeruApps.Entity.model;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;

namespace Prueba.PeruApps.Util
{
    class RESTUtil
    {
        public static bool EnviarCorreo(MailModel correo)
        {
            try
            {
                //Log.WriteLogInfo("Inicio Enviar correo");

                //tomar los datos de conexion del webconfig
                string flgenviarcorreo = ConfigurationManager.AppSettings["FlgEnviarCorreo"];

                if (flgenviarcorreo != "1")
                {
                    //Escribir en log
                    if (correo.Para.Count > 0)
                    {
                        string paranoenviar = correo.Para[0];
                        //log.Info("No enviar mensaje: " + correo.Para[0] + " - " + correo.MensajeTexto);
                    }
                    else
                    {
                        //Log.WriteLogInfo("No hay destinatario: " + correo.MensajeTexto);
                    }
                    return false;
                }

                string mailuser = ConfigurationManager.AppSettings["MailUser"];
                string mailuserpass = ConfigurationManager.AppSettings["MailUserPass"];
                string mailserver = ConfigurationManager.AppSettings["MailServer"];
                string mailserverport = ConfigurationManager.AppSettings["MailServerPort"];
                string mailserverssl = ConfigurationManager.AppSettings["MailServerSSL"];

                MailMessage mail = new MailMessage();
                mail.IsBodyHtml = true;
                mail.From = new MailAddress(mailuser);

                foreach (string para in correo.Para)
                {
                    mail.To.Add(new MailAddress(para));
                }

                foreach (string copia in correo.CC)
                {
                    if (!string.IsNullOrEmpty(copia))
                    {
                        mail.CC.Add(new MailAddress(copia));
                    }
                }

                foreach (string bulkcopia in correo.BCC)
                {
                    if (!string.IsNullOrEmpty(bulkcopia))
                    {
                        mail.Bcc.Add(new MailAddress(bulkcopia));
                    }
                }

                foreach (string adjunto in correo.Adjuntos)
                {
                    if (!string.IsNullOrEmpty(adjunto))
                    {
                        if (File.Exists(adjunto))
                        {
                            mail.Attachments.Add(new Attachment(adjunto));
                        }
                    }
                }

                mail.Subject = correo.Asunto;
                mail.Body = correo.MensajeHtml;
                mail.Priority = MailPriority.High;


                string palinBody = correo.MensajeTexto;
                AlternateView plainView = AlternateView.CreateAlternateViewFromString(palinBody, null, "text/plain");

                // then we create the Html part to embed images,
                // we need to use the prefix 'cid' in the img src value
                string htmlBody = correo.MensajeHtml;
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(
                    htmlBody, null, "text/html");

                //Imagenes integradas
                foreach (ImageModel itm in correo.ImagenAdjuntas)
                {
                    LinkedResource imageResource = new LinkedResource(itm.PathFile, "image/png");
                    imageResource.ContentId = itm.Cid;
                    imageResource.TransferEncoding = TransferEncoding.Base64;
                    /*Attachment att = new Attachment(itm.PathFile);
                    att.ContentDisposition.Inline = true;
                    mail.Attachments.Add(att);
                    */
                    htmlView.LinkedResources.Add(imageResource);
                }

                // create image resource from image path using LinkedResource class..            
                //LinkedResource imageResource = new LinkedResource(
                //     correo.ImagenAdjuntas[0], "image/png");
                //imageResource.ContentId = "saludoscumple";
                //imageResource.TransferEncoding = TransferEncoding.Base64;

                // adding the imaged linked to htmlView...
                //htmlView.LinkedResources.Add(imageResource);

                // add the views
                mail.AlternateViews.Add(plainView);
                mail.AlternateViews.Add(htmlView);

                SmtpClient clienteenvio = new SmtpClient();
                clienteenvio.UseDefaultCredentials = false;
                clienteenvio.DeliveryMethod = SmtpDeliveryMethod.Network;

                clienteenvio.Credentials = new NetworkCredential(mailuser, mailuserpass);
                clienteenvio.Host = mailserver;
                clienteenvio.Port = Convert.ToInt32(mailserverport);
                clienteenvio.EnableSsl = false;
                if (mailserverssl == "1")
                {
                    clienteenvio.EnableSsl = true;
                }

                try
                {
                    clienteenvio.Send(mail);
                    //log.Info("Se envio el mensaje de correo " + mail.To[0].Address);
                    return true;
                }
                catch (Exception ex)
                {
                    //log.Error(ex.Message);
                    return false;
                }
            }
            catch (Exception ex)
            {
                //log.Error(ex.Message);
                throw ex;
            }
        }

        public static void EnviarCorreoError(Exception exception)
        {
            try
            {
                string mailSoporte = ConfigurationManager.AppSettings["MailSoporte"];

                string mensaje = "Soporte: " + "\r\n\r\n";
                if (exception.InnerException != null)
                    mensaje = String.Format("{0}Mensaje: {1}\r\n\r\nInnerException: {2}\r\n\r\n", mensaje, exception.Message, exception.InnerException.InnerException.Message);
                else
                    mensaje = String.Format("{0}Mensaje: {1}\r\n\r\n", mensaje, exception.Message);

                mensaje = String.Format("{0}StackTrace: {1}\r\n\r\n", mensaje, exception.StackTrace);
                mensaje = String.Format("{0}Site: {1}\r\n\r\n", mensaje, exception.TargetSite);
                mensaje = String.Format("{0}Url: {1}", mensaje, exception.Source);

                if (mailSoporte != string.Empty)
                {
                    string[] mailsSoporte = mailSoporte.Split(';');
                    if (mailsSoporte.Length > 0)
                    {
                        MailModel mailmessage = new MailModel();
                        mailmessage.Asunto = Constantes.AsuntoSoporte;
                        mailmessage.MensajeTexto = mensaje;
                        mailmessage.MensajeHtml = mensaje;
                        foreach (string correo in mailsSoporte)
                        {
                            mailmessage.Para.Add(correo);
                        }

                        EnviarCorreo(mailmessage);
                    }
                }
            }
            catch (Exception ex)
            {
                //Log.WriteLogError(ex.Message);
            }
        }

        /*
            public static string ResolveServerUrl(string serverUrl, bool forceHttps)
            {
                if (serverUrl.IndexOf("://") > -1)
                    return serverUrl;

                string newUrl = serverUrl;
                Uri originalUri = System.Web.HttpContext.Current.Request.Url;
                newUrl = (forceHttps ? "https" : originalUri.Scheme) +
                    "://" + originalUri.Authority + newUrl;
                return newUrl;
            }
            */

        public static DateTime? ConvertFecha(string dfecha)
        {
            try
            {
                string[] cadena = dfecha.Split('/');

                int anio = int.Parse(cadena[2]);
                int mes = int.Parse(cadena[1]);
                int dia = int.Parse(cadena[0]);

                return new DateTime(anio, mes, dia);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static DateTime ConvertHora(string chora)
        {
            try
            {
                string[] cadena = chora.Split(':');

                int hora = int.Parse(cadena[0]);
                int minutos = int.Parse(cadena[1]);


                return new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, hora, minutos, 0);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
