﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prueba.PeruApps.Entity;
using Prueba.PeruApps.Entity.model;
using System.Data;
using System.Data.SqlClient;

namespace Prueba.PeruApps.DataAccess
{
    public class DLAbogado
    {
        private BDConnection BDConnection = new BDConnection();

        public List<AbogadoQuery> ListarAbogados()
        {
            try
            {
                List<AbogadoQuery> resul = new List<AbogadoQuery>();

                string strsql = "usp_ListarAbogados";
                List<SqlParameter> parametros = new List<SqlParameter>();
                SqlDataReader reader = BDConnection.GetUtilBD().GetReader(strsql, null);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        AbogadoQuery obj = new AbogadoQuery();

                        obj.id = Convert.ToInt32(reader["ID"]);
                        obj.especialidadDesc = Convert.ToString(reader["ESPECIALIDAD"]);
                        obj.apePaterno = Convert.ToString(reader["APPATERNO"]);
                        obj.apeMaterno = Convert.ToString(reader["APMATERNO"]);
                        obj.nombres = Convert.ToString(reader["NOMBRES"]);
                        obj.tipoDocumentoDesc = Convert.ToString(reader["TIPODOCUMENTO"]);
                        obj.nroDoc = Convert.ToString(reader["NRODOCUMENTO"]);
                        obj.codColegioAb = Convert.ToString(reader["COD_COLEGIOAB"]);

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BDConnection.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Especialidad> ListarEspecialidades()
        {
            try
            {
                List<Especialidad> resul = new List<Especialidad>();

                string strsql = "usp_ListarEspecialidades";
                List<SqlParameter> parametros = new List<SqlParameter>();
                SqlDataReader reader = BDConnection.GetUtilBD().GetReader(strsql, null);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        Especialidad obj = new Especialidad();

                        obj.id = Convert.ToInt32(reader["ID"]);
                        obj.descripcion = Convert.ToString(reader["DESCRIPCION"]);

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BDConnection.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TipoDocumento> ListarTiposDocumento()
        {
            try
            {
                List<TipoDocumento> resul = new List<TipoDocumento>();

                string strsql = "usp_ListarTipoDocumentos";
                List<SqlParameter> parametros = new List<SqlParameter>();
                SqlDataReader reader = BDConnection.GetUtilBD().GetReader(strsql, null);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        TipoDocumento obj = new TipoDocumento();

                        obj.id = Convert.ToInt32(reader["ID"]);
                        obj.descripcion = Convert.ToString(reader["DESCRIPCION"]);

                        resul.Add(obj);
                    }
                }
                reader.Close();
                BDConnection.CloseConexion();

                return resul;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AdicionarModificarAbogado(Abogado obj)
        {
            try
            {
                if (obj.id == 0)
                {
                    string strsql = "usp_AdicionarAbogado";
                    List<SqlParameter> parametros = new List<SqlParameter>();
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@ESPECIALIDAD_ID", System.Data.SqlDbType.Int, obj.especialidadId));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@APPATERNO", System.Data.SqlDbType.VarChar, 30, obj.apePaterno));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@APMATERNO", System.Data.SqlDbType.VarChar, 30, obj.apeMaterno));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@NOMBRES", System.Data.SqlDbType.VarChar, 60, obj.nombres));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@TIPO_DOCUMENTO", System.Data.SqlDbType.Int, obj.tipoDocumento));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@NRODOCUMENTO", System.Data.SqlDbType.VarChar, 20, obj.nroDoc));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@COD_COLEGIOAB", System.Data.SqlDbType.VarChar, 30, obj.codColegioAb));
                    BDConnection.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                }
                else
                {
                    string strsql = "usp_ModificarAbogado";
                    List<SqlParameter> parametros = new List<SqlParameter>();
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@ID", System.Data.SqlDbType.Int, obj.id));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@ESPECIALIDAD_ID", System.Data.SqlDbType.Int, obj.especialidadId));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@APPATERNO", System.Data.SqlDbType.VarChar, 30, obj.apePaterno));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@APMATERNO", System.Data.SqlDbType.VarChar, 30, obj.apeMaterno));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@NOMBRES", System.Data.SqlDbType.VarChar, 60, obj.nombres));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@TIPO_DOCUMENTO", System.Data.SqlDbType.Int, obj.tipoDocumento));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@NRODOCUMENTO", System.Data.SqlDbType.VarChar, 20, obj.nroDoc));
                    parametros.Add(BDConnection.GetUtilBD().CreateParameter("@COD_COLEGIOAB", System.Data.SqlDbType.VarChar, 30, obj.codColegioAb));
                    BDConnection.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ValidacionPreviaAdicion(Abogado obj)
        {
            try
            {
                int codeResult = 0;
                string strsql = "usp_ValidacionPreviaAdicion";
                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter parametroSalida = BDConnection.GetUtilBD().CreateParameter("@COD_RESULT", System.Data.SqlDbType.Int, codeResult);
                parametroSalida.Direction = ParameterDirection.InputOutput;

                parametros.Add(parametroSalida);
                parametros.Add(BDConnection.GetUtilBD().CreateParameter("@TIPO_DOCUMENTO", System.Data.SqlDbType.Int, obj.tipoDocumento));
                parametros.Add(BDConnection.GetUtilBD().CreateParameter("@NRODOCUMENTO", System.Data.SqlDbType.VarChar, 20, obj.nroDoc));
                parametros.Add(BDConnection.GetUtilBD().CreateParameter("@COD_COLEGIOAB", System.Data.SqlDbType.VarChar, 30, obj.codColegioAb));

                BDConnection.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                codeResult = Convert.ToInt32(parametroSalida.Value);

                return codeResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ValidacionPreviaModificacion(Abogado obj)
        {
            try
            {
                int codeResult = 0;
                string strsql = "usp_ValidacionPreviaModificacion";
                List<SqlParameter> parametros = new List<SqlParameter>();

                SqlParameter parametroSalida = BDConnection.GetUtilBD().CreateParameter("@COD_RESULT", System.Data.SqlDbType.Int, codeResult);
                parametroSalida.Direction = ParameterDirection.InputOutput;

                parametros.Add(parametroSalida);
                parametros.Add(BDConnection.GetUtilBD().CreateParameter("@ID", System.Data.SqlDbType.Int, obj.id));
                parametros.Add(BDConnection.GetUtilBD().CreateParameter("@TIPO_DOCUMENTO", System.Data.SqlDbType.Int, obj.tipoDocumento));
                parametros.Add(BDConnection.GetUtilBD().CreateParameter("@NRODOCUMENTO", System.Data.SqlDbType.VarChar, 20, obj.nroDoc));
                parametros.Add(BDConnection.GetUtilBD().CreateParameter("@COD_COLEGIOAB", System.Data.SqlDbType.VarChar, 30, obj.codColegioAb));

                BDConnection.GetUtilBD().ExecuteNonQuery(strsql, parametros);
                codeResult = Convert.ToInt32(parametroSalida.Value);

                return codeResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AbogadoQuery ObtenerAbogadoPorId(int id)
        {
            try
            {
                AbogadoQuery obj = null;

                string strsql = "usp_ObtenerAbogadoPorId";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BDConnection.GetUtilBD().CreateParameter("@ID", System.Data.SqlDbType.Int, id));
                SqlDataReader reader = BDConnection.GetUtilBD().GetReader(strsql, parametros);

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        obj = new AbogadoQuery();

                        obj.id = Convert.ToInt32(reader["ID"]);
                        obj.especialidadId = Convert.ToInt32(reader["ESPECIALIDAD_ID"]);
                        obj.especialidadDesc = Convert.ToString(reader["ESPECIALIDAD"]);
                        obj.apePaterno = Convert.ToString(reader["APPATERNO"]);
                        obj.apeMaterno = Convert.ToString(reader["APMATERNO"]);
                        obj.nombres = Convert.ToString(reader["NOMBRES"]);
                        obj.tipoDocumentoId = Convert.ToInt32(reader["TIPODOCUMENTO_ID"]);
                        obj.tipoDocumentoDesc = Convert.ToString(reader["TIPODOCUMENTO"]);
                        obj.nroDoc = Convert.ToString(reader["NRODOCUMENTO"]);
                        obj.codColegioAb = Convert.ToString(reader["COD_COLEGIOAB"]);

                        break;
                    }
                }
                reader.Close();
                BDConnection.CloseConexion();

                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarAbogado(int id)
        {
            try
            {
                string strsql = "usp_EliminarAbogado";
                List<SqlParameter> parametros = new List<SqlParameter>();
                parametros.Add(BDConnection.GetUtilBD().CreateParameter("@ID", System.Data.SqlDbType.Int, id));
                BDConnection.GetUtilBD().ExecuteNonQuery(strsql, parametros);

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


    }
}
