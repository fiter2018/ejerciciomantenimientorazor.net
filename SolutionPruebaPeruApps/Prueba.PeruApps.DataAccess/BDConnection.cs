﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BAZ.GEN.LibSqlUtil;
using System.Configuration;

namespace Prueba.PeruApps.DataAccess
{
    public class BDConnection
    {
        private string ConnectionString = string.Empty;
        private BDConexion BDConexion;
        private UtilBD UtilBD;

        public BDConexion GetBDConexion()
        {
            try
            {
                if (BDConexion == null)
                {
                    BDConexion = new BDConexion(GetConnectionString());
                }
                return BDConexion;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void CloseConexion()
        {
            try
            {
                BDConexion.CloseConnection();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public UtilBD GetUtilBD()
        {
            try
            {
                if (UtilBD == null)
                {
                    UtilBD = new UtilBD(GetBDConexion());
                }

                return UtilBD;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        private string GetConnectionString()
        {

            try
            {
                if (ConnectionString == string.Empty)
                {
                    ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["ESTUDIO"].ConnectionString;
                    if (ConnectionString == string.Empty)
                    {
                        throw new Exception("No se encontro la llave para la cadena de conexion o esta vacía");
                    }
                    else
                    {
                        return ConnectionString;
                    }
                }
                else
                {
                    return ConnectionString;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
