﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prueba.PeruApps.Client;
using Prueba.PeruApps.Entity;
using Prueba.PeruApps.Entity.model;

namespace Prueba.PeruApps.Business
{
    public class BLAbogado
    {

        private RESTClient oRESTClient;

        public BLAbogado()
        {
            oRESTClient = new RESTClient();
        }
       
        public List<AbogadoQuery> ListarAbogados()
        {
            try
            {
                return oRESTClient.Get<List<AbogadoQuery>>(string.Format("abogado/listarAbogados"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<Especialidad> ListarEspecialidades()
        {
            try
            {
                return oRESTClient.Get<List<Especialidad>>(string.Format("abogado/listarEspecialidades"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<TipoDocumento> ListarTiposDocumento()
        {
            try
            {
                return oRESTClient.Get<List<TipoDocumento>>(string.Format("abogado/listarTiposDocumento"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AdicionarModificarAbogado(Abogado obj)
        {
            try
            {
                return Convert.ToBoolean(oRESTClient.Post<Abogado>("abogado/adicionarModificar", obj));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ValidacionPreviaAdicion(Abogado obj)
        {
            try
            {
                return Convert.ToInt32(oRESTClient.Post<Abogado>("abogado/validacionPreviaAdicion", obj));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int ValidacionPreviaModificacion(Abogado obj)
        {
            try
            {
                return Convert.ToInt32(oRESTClient.Post<Abogado>("abogado/validacionPreviaModificacion", obj));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public AbogadoQuery ObtenerAbogadoPorId(int id)
        {
            try
            {
                return oRESTClient.Get<AbogadoQuery>(string.Format("abogado/get/{0}", Convert.ToString(id)));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool EliminarAbogado(Abogado obj)
        {
            try
            {
                return Convert.ToBoolean(oRESTClient.Post<Abogado>("abogado/eliminarAbogado", obj));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
    }
}
