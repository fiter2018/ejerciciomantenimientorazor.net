﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Prueba.PeruApps.DataAccess;
using Prueba.PeruApps.Entity;
using Prueba.PeruApps.Entity.model;

namespace Prueba.PeruApps.Service.Controllers
{
    [RoutePrefix("api/abogado")]
    public class AbogadoController : ApiController
    {
        
        private DLAbogado dlAbogado;

        public AbogadoController()
        {
            dlAbogado = new DLAbogado();
        }

        [HttpGet]
        [Route("listarAbogados")]
        public IHttpActionResult ListarAbogados()
        {
            List<AbogadoQuery> resul = dlAbogado.ListarAbogados();

            return Ok(resul);
        }

        [HttpGet]
        [Route("listarEspecialidades")]
        public IHttpActionResult ListarEspecialidades()
        {
            List<Especialidad> resul = dlAbogado.ListarEspecialidades();

            return Ok(resul);
        }

        [HttpGet]
        [Route("listarTiposDocumento")]
        public IHttpActionResult ListarTiposDocumento()
        {
            List<TipoDocumento> resul = dlAbogado.ListarTiposDocumento();

            return Ok(resul);
        }

        [HttpGet]
        [Route("get/{id}")]
        public IHttpActionResult ObtenerAbogadoPorId(string id)
        {
            AbogadoQuery item = dlAbogado.ObtenerAbogadoPorId(Convert.ToInt32(id));
            return Ok(item);
        }

        [HttpPost]
        [Route("adicionarModificar")]
        public IHttpActionResult AdicionarModificarAbogado(Abogado obj)
        {
            //TODO COMPLETAR USER
            bool resul = dlAbogado.AdicionarModificarAbogado(obj);

            return Ok(resul);
        }

        [HttpPost]
        [Route("validacionPreviaAdicion")]
        public IHttpActionResult ValidacionPreviaAdicion(Abogado obj)
        {
            try
            {
                int codeResult = 0;

                codeResult = dlAbogado.ValidacionPreviaAdicion(obj);

                return Ok(codeResult);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("validacionPreviaModificacion")]
        public IHttpActionResult ValidacionPreviaModificacion(Abogado obj)
        {
            try
            {
                int codeResult = 0;

                codeResult = dlAbogado.ValidacionPreviaModificacion(obj);

                return Ok(codeResult);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        [Route("eliminarAbogado")]
        public IHttpActionResult EliminarAbogado(Abogado obj)
        {
            bool resul = dlAbogado.EliminarAbogado(obj.id);

            return Ok(resul);
        }

    }
}
