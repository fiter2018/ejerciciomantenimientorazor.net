﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prueba.PeruApps.WebMVC.Models
{
    public class AbogadoModel
    {
        public int id { get; set; }
        public int especialidadId { get; set; }
        public string apePaterno { get; set; }
        public string apeMaterno { get; set; }
        public string nombres { get; set; }
        public int tipoDocumento { get; set; }
        public string nroDoc { get; set; }
        public string codColegioAb { get; set; }
        public int estado { get; set; }
        public DateTime? fchCreacion { get; set; }
        public DateTime? fchEdicion { get; set; }
    }
}