﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Prueba.PeruApps.Business;
using Prueba.PeruApps.Entity;
using Prueba.PeruApps.Entity.model;

namespace Prueba.PeruApps.WebMVC.Controllers
{
    public class AbogadoController : Controller
    {
        BLAbogado oBLAbogado = new BLAbogado();

        // GET: Abogado
        public ActionResult Index()
        {            
            return View(oBLAbogado.ListarAbogados());
        }

        // GET: Abogado/Details/5
        public ActionResult Details(int id)
        {
            return View(oBLAbogado.ObtenerAbogadoPorId(id));
        }

        // GET: Abogado/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Abogado/Create
        [HttpPost]
        public ActionResult Create(Abogado obj)
        {
            try
            {
                if (obj.id == 0)
                {  /* Adicionar */
                    int resultValAdic = oBLAbogado.ValidacionPreviaAdicion(obj);
                    if (resultValAdic == Constantes.ErrorNroDocOcodColegRepetido)
                    {
                        return Json(new { result = Enums.eCodeError.ERROR, message = "Ya hay un abogado registrado con el nroDoc y/o codColegiatura ingresados" });
                    }
                }
                else
                {  /* Modificar */
                    int resultValModif = oBLAbogado.ValidacionPreviaModificacion(obj);
                    if (resultValModif == Constantes.ErrorNroDocOcodColegRepetido)
                    {
                        return Json(new { result = Enums.eCodeError.ERROR, message = "Ya hay un abogado registrado con el nroDoc y/o codColegiatura ingresados" });
                    }
                }

                bool rest = oBLAbogado.AdicionarModificarAbogado(obj);

                RedirectToAction("Index");
                return Json(new { result = Enums.eCodeError.OK, message = "Guardado/Actualizado Satisfactoriamente" });
            }
            catch
            {
                return View();
            }
        }

        // GET: Abogado/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Abogado/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Abogado/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Abogado/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                Abogado abo = new Abogado();
                abo.id = id;
                bool result = oBLAbogado.EliminarAbogado(abo);

                return Json(new { result = Enums.eCodeError.OK, message = "Eliminado Satisfactoriamente" });
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ObtenerAbogadoPorId(string id)
        {
            AbogadoQuery item = oBLAbogado.ObtenerAbogadoPorId(Convert.ToInt32(id));

            return Json(new { result = Enums.eCodeError.OK, data = item }, JsonRequestBehavior.AllowGet);
        }

    }
}
