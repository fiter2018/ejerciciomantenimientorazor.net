﻿var flagEnviar = 0;
var showed;
var inAddUserMode = false;
var inEditUserMode = false;

$(function () {

    $("#btnNuevo").on("click", function (e) {
        fnLimpiarControles();
        $("#divPrincipalAbo").modal("show");
    });

    $("#btnGuardar").on("click", function (e) {
        fnGuardar();
    });

    $(".btnEditar").on("click", function (e) {
        fnEditar();
    });

    $(".btnEliminar").on("click", function (e) {
        fnEliminar();
    });

    $("#frmPrincipalAbo").on("submit", function (e) {
        e.preventDefault();
    });

    validarControlesFormulario();

});
//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
function validarControlesFormulario() {
    $("#frmPrincipalAbo").validate({
        rules: {
            "apePaterno": {
                required: true,
                minlength: 4
            },
            "apeMaterno": {
                required: true,
                minlength: 4
            },
            "nombres": {
                required: true,
                minlength: 4
            },
            "nroDoc": {
                required: true,
                minlength: 8
            },
            "codColegioAb": {
                required: true,
                minlength: 4
            }
        }
    });
}

function fnRefrescar() {
    location.reload(true);
}

function fnGuardar() {
    var params = $("#frmPrincipalAbo").serialize();

    if ($("#frmPrincipalAbo").valid()) {
        Post("Abogado/Create", params).done(function (response) {
            if (response.result == 0) {
                fnAlertar("Información", response.message, function () {
                    $("#divPrincipalAbo").modal("hide");
                    fnRefrescar();
                });
            } else {
                fnAlertar("Alerta", response.message);
            }
        });
    }
}

function fnEditar(id) {

    Get("Abogado/ObtenerAbogadoPorId/" + id).done(function (response) {
        if (response.result == 0) {
            $("#idAbogado").val(id);

            $("#ddlEspecialidades").val(response.data.especialidadId);
            $("#txtApPaterno").val(response.data.apePaterno);
            $("#txtApMaterno").val(response.data.apeMaterno);
            $("#txtNombres").val(response.data.nombres);
            $("#ddlTiposDocumento").val(response.data.tipoDocumentoId);
            $("#txtNroDocumento").val(response.data.nroDoc);
            $("#txtCodColegio").val(response.data.codColegioAb);

            $("#divPrincipalAbo").modal("show");
        }
    });
}

function fnEliminar(id) {
    var params = new Object();
    params.id = id;

    fnConfirmar("Eliminar", "¿Seguro que desea eliminar?", function () {
        Post("Abogado/Delete", params).done(function (response) {
            if (response.result == 0) {
                fnRefrescar();
            }
        });
    });
}

function fnLimpiarControles() {
    $("#ddlEspecialidades").val("");
    $("#idAbogado").val("-");
    $("#txtApPaterno").val("");
    $("#txtApMaterno").val("");
    $("#txtNombres").val("");
    $("#ddlTiposDocumento").val("");
    $("#txtNroDocumento").val("");
    $("#txtCodColegio").val("");

}
