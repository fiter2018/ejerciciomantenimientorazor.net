﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.PeruApps.Entity.model
{
    public class AbogadoQuery
    {
        public int id { get; set; }
        public int especialidadId { get; set; }
        public string especialidadDesc { get; set; }
        public string apePaterno { get; set; }
        public string apeMaterno { get; set; }
        public string nombres { get; set; }
        public int tipoDocumentoId { get; set; }
        public string tipoDocumentoDesc { get; set; }
        public string nroDoc { get; set; }
        public string codColegioAb { get; set; }
    }
}
