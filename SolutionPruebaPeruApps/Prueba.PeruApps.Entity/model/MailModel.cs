﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.PeruApps.Entity.model
{
    public class MailModel
    {
        public List<string> Para { get; set; }
        public List<string> CC { get; set; }
        public List<string> BCC { get; set; }
        public string MensajeHtml { get; set; }
        public string MensajeTexto { get; set; }
        public string Asunto { get; set; }
        public List<ImageModel> ImagenAdjuntas { get; set; }
        public List<string> Adjuntos { get; set; }

        public MailModel()
        {
            Para = new List<string>();
            CC = new List<string>();
            BCC = new List<string>();
            ImagenAdjuntas = new List<ImageModel>();
            Adjuntos = new List<string>();

        }

    }

    public class ImageModel
    {
        public string FileName { get; set; }
        public string Cid { get; set; }
        public string PathFile { get; set; }
    }
    
}
