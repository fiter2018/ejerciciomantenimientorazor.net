﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.PeruApps.Entity
{
    public class Constantes
    {
        public const int ErrorNroDocOcodColegRepetido = 1;

        public const int ErrorUsuarioMozo = 3;
        public const string AsuntoSoporte = "Soporte Prueba";
    }
}
