﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prueba.PeruApps.Entity
{
    public class Enums
    {
        public enum eCodeError : int
        {
            OK = 0,
            ERROR = 1,
            VAL = 2
        }

        public enum eStatus : int
        {
            ACTIVE = 1,
            INACTIVE = 2
        }

    }
}
